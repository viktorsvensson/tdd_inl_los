package org.example.service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.example.entity.AppUser;
import org.example.repo.AppUserRepo;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.time.Instant;
import java.util.Date;

public class AppUserService {

    private final static String SECRET = System.getenv("JWT_SECRET");
    private final static SignatureAlgorithm ALGORITHM = SignatureAlgorithm.HS256;
    private final static Key KEY =
            new SecretKeySpec(SECRET.getBytes(), ALGORITHM.getJcaName());
    private final static int EXPIRATION_SECONDS = 300;

    private final AppUserRepo appUserRepo;

    public AppUserService(AppUserRepo appUserRepo) {
        this.appUserRepo = appUserRepo;
    }

    public String login(String username, String password) {
        AppUser appUser = appUserRepo.findByUsername(username).orElseThrow();
        if(appUser.getPassword().equals(password))
                return buildJws(username);

        throw new RuntimeException("Wrong Password");
    }

    private static String buildJws(String username){
        return Jwts.builder()
                .signWith(KEY)
                .setSubject(username)
                .setExpiration(Date.from(Instant.now().plusSeconds(EXPIRATION_SECONDS)))
                .compact();
    }
}
