package org.example.entity;

import org.example.entity.AppUser;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AppUserTest {

    @Test
    public void constructor(){
        // Given
        String username = "anna";
        String password = "1234";
        int id = 1;


        // When
        AppUser appUser = new AppUser(id, username, password);

        // Then
        assertNotNull(appUser);
    }

    @Test
    public void getters(){
        // Given
        String username = "anna";
        String password = "1234";
        int id = 1;
        AppUser appUser = new AppUser(id, username, password);

        // When
        String retrievedUsername = appUser.getUsername();
        String retrievedPassword = appUser.getPassword();
        int retrievedId = appUser.getId();

        // Then
        assertEquals(username, retrievedUsername);
        assertEquals(password, retrievedPassword);
        assertEquals(id, retrievedId);
    }

    @Test
    public void setters_UsernameAndPassword(){
        // Given
        String usernameToChangeTo = "anna";
        String passwordToChangeTo = "1234";
        int id = 1;
        AppUser appUser = new AppUser(id, "", "");

        // When
        appUser.setUsername(usernameToChangeTo);
        appUser.setPassword(passwordToChangeTo);

        // Then
        assertEquals(usernameToChangeTo, appUser.getUsername());
        assertEquals(passwordToChangeTo, appUser.getPassword());
    }

    @Test
    public void equals_WithEqualId_ShouldCountAsEqual(){
        // Given
        AppUser appUser1 = new AppUser(1, "Alice", "1234");
        AppUser appUser2 = new AppUser(1, "Bob", "ABCD");

        // Then
        assertEquals(appUser1, appUser2);
    }

}
