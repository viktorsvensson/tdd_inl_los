package org.example.service;

import io.jsonwebtoken.*;
import org.example.entity.AppUser;
import org.example.repo.AppUserRepo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AppUserServiceTest {

    private final static String SECRET = System.getenv("JWT_SECRET");
    private final static SignatureAlgorithm ALGORITHM = SignatureAlgorithm.HS256;
    private final static Key KEY =
            new SecretKeySpec(SECRET.getBytes(), ALGORITHM.getJcaName());
    private final static int EXPIRATION_SECONDS = 300;

    @Mock
    AppUserRepo appUserRepo;

    private AppUserService appUserService;

    @BeforeEach
    public void setUp(){
        appUserService = new AppUserService(appUserRepo);
    }

    @Test
    public void login_withCorrectCredentials_shouldReturnJWT(){
        // Given
        int id = 1;
        String username = "anna";
        String password = "1234";

        // When
        when(appUserRepo.findByUsername(username))
                .thenReturn(Optional.of(new AppUser(id, username, password)));

        String jwtToken = appUserService.login(username, password);

        Jws<Claims> jwt = Jwts.parserBuilder()
                .setSigningKey(KEY)
                .build()
                .parseClaimsJws(jwtToken);


        // Then
        assertEquals(username, jwt.getBody().getSubject());
        assertEquals(ALGORITHM.getValue(), jwt.getHeader().getAlgorithm());
        assertTrue(
                Duration.between(
                    Instant.now().plusSeconds(EXPIRATION_SECONDS),
                    jwt.getBody().getExpiration().toInstant()
                ).toSeconds() < 10
        );
    }

}
