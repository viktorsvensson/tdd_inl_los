package org.example.repo;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.junit.jupiter.api.Assertions.*;

@Testcontainers
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@Sql(scripts = {"/seedAppUser.sql"})
public class UserRepoIntegrationTest {

    @Autowired
    AppUserRepo appUserRepo;

    @Container
    static PostgreSQLContainer<?> db =
            new PostgreSQLContainer<>("postgres:15");

    @DynamicPropertySource
    public static void props(DynamicPropertyRegistry registry){
        registry.add("spring.datasource.url", db::getJdbcUrl);
        registry.add("spring.datasource.username", db::getUsername);
        registry.add("spring.datasource.password", db::getPassword);
        registry.add("spring.datasource.driver-class-name", db::getDriverClassName);
        registry.add("spring.jpa.hibernate.ddl-auto", () -> "create-drop");
    }

    @BeforeAll
    static void beforeAll(){
        db.start();
    }

    @Test
    public void findUserById(){
        assertDoesNotThrow(() -> appUserRepo.findById(1).orElseThrow());
    }

    @Test
    public void findUserByUserName(){
        assertDoesNotThrow(
                () -> appUserRepo.findByUsername("anna").orElseThrow()
        );
    }

}
